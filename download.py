import datetime
import wget
import os
import wget
import time
if __name__ == '__main__':
    urls = open('url.txt').read().split('\n')

    N = len(urls)
    for i in xrange(N):
        path = os.path.join('data', str(i).zfill(4))
        if not os.path.exists(path):
            os.mkdir(path)

    while (True):
        for i, url in enumerate(urls):
            now = str(datetime.datetime.now())
            path = os.path.join('data', str(i).zfill(4),  now + '.jpg')
            wget.download(url, out=path)
        time.sleep(60)



